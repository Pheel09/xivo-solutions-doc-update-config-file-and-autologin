************************
Upgrade Electra to Freya
************************

In this section is listed the manual steps to do when migrating from Electra to Freya.

.. contents::

.. warning:: For Freya Debian was upgraded to Debian 10 (Buster).

   Therefore:

     * the upgrade to Freya will take longer than usual
     * upgrade from older version than XiVO Boréalis (2018.16) are not supported (you need first to upgrade
       to a version above Boréalis before being able to upgrade to Freya).

   Please read carefully :ref:`Debian 10 (Buster) upgrade notes <debian_10_upgrade_notes>` page.

   Note also that upgrade to Debian 10 on MDS and XiVO CC is manual (see manual procedure below).

.. warning:: **XiVO CC/UC:** After upgrade you need to launch postgres upgrade (from 9.4 to version 11).

.. warning:: **Known Upgrade Limitations:** as Freya.02 release these are the known limitations for upgrade.

   * UC Addon:

     * You can't use UC Assistant and Admin Webi together (see #3522)


Before Upgrade
==============

On XiVO PBX
-----------

* Debian system will be upgraded to Debian 10 (Buster)

  .. important:: Please **read carefully** the :ref:`Before the upgrade <debian_10_upgrade_notes_before>` section in the Debian 10 (Buster) upgrade notes.


On XiVO CC
----------

* Reporting: During upgrade all **Kibana configuration** (including the dashboard) will be lost (it is stored in *elasticsearch* container).
  You **MUST** :ref:`backup Kibana configuration <xivocc_backup_kibana>` before the upgrade.


On MDS
------

* Upgrade system to Debian 10 (Buster) with the following manual procedure:

  #. Run upgrade

     .. code-block:: bash

      apt update
      sed -i 's/stretch/buster/' /etc/apt/sources.list /etc/apt/sources.list.d/docker.list /etc/apt/sources.list.d/pgdg.list
      cat >> /etc/apt/sources.list <<EOF
      # buster-backports, previously on backports.debian.org
      deb http://ftp.fr.debian.org/debian buster-backports main
      deb-src http://ftp.fr.debian.org/debian buster-backports main
      EOF
      apt update

      export DEBIAN_FRONTEND=noninteractive
      export APT_LISTCHANGES_FRONTEND=none
      force_yes="--allow-downgrades --allow-remove-essential --allow-change-held-packages"

      echo "Preparing dahdi-linux-dkms upgrade..."
      dkms remove -m dahdi-linux -v $(LANG='C' apt-cache policy dahdi-linux-dkms | grep Installed | grep -oE '2\.11\.1.+') --all
      apt install --yes ${force_yes} -o Dpkg::Options::="--force-confnew" linux-image-amd64 linux-headers-amd64
      apt autoremove --yes

      echo "Executing XiVO PBX minimal upgrade actions..."
      apt upgrade --yes ${force_yes} -o Dpkg::Options::="--force-confnew"
      echo "Executing XiVO PBX full upgrade actions..."
      apt dist-upgrade --yes ${force_yes} -o Dpkg::Options::="--force-confnew"
      apt autoremove --yes

      xivo-configure-uuid

  #. Check GRUB **before rebooting**

     .. code-block:: bash

      install_device=$(debconf-show grub-pc | grep 'grub-pc/install_devices:' | cut -b3- | cut -f2 -d' ' | cut -d',' -f1)
      if [ "$install_device" -a ! -e "$install_device" ]; then
      echo -e "\e[1;31mYou must install GRUB BEFORE rebooting\e[0m"
      fi

     If it is broken, fix it:

     .. code-block:: bash

      apt-get install grub-pc
      rm /boot/grub/device.map
      grub-install /dev/<boot_device>
      dpkg-reconfigure grub-pc

  #. Reboot the MDS

* Install mds-upgrade *package* (containing mds-upgrade *script*):

    #. Switch version using ``xivo-dist`` utility and specifying the LTS or specific version you want to upgrade to. For example::

        xivo-dist xivo-freya

    #. Update package list and install mds-upgrade::

        apt-get update
        apt-get install mds-upgrade

    #. Then follow the remaining step of the updated :ref:`xds_mds-upgrade` procedure. (Run the ``mds-upgrade`` script.)

After Upgrade
=============

On XiVO PBX
-----------

* Debian system will be upgraded to Debian 10 (Buster)

  .. warning:: Please **read carefully** the :ref:`After the upgrade <debian_10_upgrade_notes_after>` section in the Debian 10 (Buster) upgrade notes.

* You must add additional permissions to already existing *Webi administrators* who should have access to
  *Users* menu and the new *User Labels* menu. Follow the :ref:`webi_admin_users` and :ref:`labels` documentation.

* XDS:

  * You must configure :ref:`the SMTP relay on XiVO Main <xds_xivo-smtp-relay>` to be able to have the email notification for voicemail on MDS (see MDS part below).

On XiVO CC/UC
-------------

.. important:: **Database** after the upgrade you MUST upgrade pgxivocc from 9.4 to version 11.

    To do this, launch the script (it will restart the pgxivocc container).
    On XiVO CC::

       xivocc-migrate-db-94-to-11

    Or on XiVO with UC addon::

       xivouc-migrate-db-94-to-11

    If you are installing the chat backend, you must upgrade pgxivocc first and then start the chat backend installation.

* If the chat backend was already installed before the upgrade, delete channels between surveybot user
  and other users. Messages from surveybot are not handled correctly and cause errors in logs.

  #. Get channel ids from the mattermost database:

     .. code-block:: sql

        psql -h localhost -p 5443 -U postgres mattermost -c "
        SELECT channelid FROM posts
        JOIN users ON users.id = posts.userid
        WHERE users.username = 'surveybot'"

  #. Delete them safely from the mattermost CLI. For example:

     .. code-block:: bash

        xivocc-dcomp exec mattermost mattermost-cli channel delete "
        >  h37fet418iff3pdpfcj6neht4h
        >  esu8swozjtnhm8asordwk56jmo"

  #. Confirm deletion by typing "YES"

On XiVO CC only
^^^^^^^^^^^^^^^

* Upgrade system to Debian 10 (Buster) with the following manual procedure:

  #. Run upgrade

     .. code-block:: bash

      sed -i 's/stretch/buster/' /etc/apt/sources.list /etc/apt/sources.list.d/docker.list
      apt update

      export DEBIAN_FRONTEND=noninteractive
      export APT_LISTCHANGES_FRONTEND=none
      force_yes="--allow-downgrades --allow-remove-essential --allow-change-held-packages"

      echo "Executing XiVO CC minimal upgrade actions..."
      apt upgrade --yes ${force_yes} -o Dpkg::Options::="--force-confnew"
      echo "Executing XiVO CC full upgrade actions..."
      apt dist-upgrade --yes ${force_yes} -o Dpkg::Options::="--force-confnew"
      apt autoremove --yes

  #. Check GRUB **before rebooting**

     .. code-block:: bash

      install_device=$(debconf-show grub-pc | grep 'grub-pc/install_devices:' | cut -b3- | cut -f2 -d' ' | cut -d',' -f1)
      if [ "$install_device" -a ! -e "$install_device" ]; then
      echo -e "\e[1;31mYou must install GRUB BEFORE rebooting\e[0m"
      fi

     If it is broken, fix it:

     .. code-block:: bash

      apt-get install grub-pc
      rm /boot/grub/device.map
      grub-install /dev/<boot_device>
      dpkg-reconfigure grub-pc

  #. Reboot

* Reporting:

  * :ref:`Restore Kibana configuration <xivocc_restore_kibana>`.
  * The last 7 days of data will be re-replicated to Elasticsearch, see :ref:`totem_data_flow`.
    It may take some time if you have a huge amount of calls per week (more than 1 hour if you have 2 million of queue_log per week).

* **XDS installation only**: you need to update the nginx configuration for WebRTC on MDS if you had already followed the :ref:`xds_webrtc-configuration`

  #. On your XiVO CC hosting the nginx server, edit the file :file:`/etc/docker/nginx/sip_proxy/sip_proxy.conf`
     (if this file doesn't exist on your server you can skip this)
  #. Change the location ``location /ws-MDS_NAME`` to ``location /wssip-MDS_NAME`` (notice change from `ws` to `wssip`)
  #. Inside the location,

     #. add the directive ``auth_request /validatetoken;``
     #. and change the ``proxy_{connect,read,send}_timeout`` parameters as shown
  #. Below is an example before/after with mds1 as MDS_NAME:

   +------------------------------------------+------------------------------------------+
   | Before                                   | After                                    |
   +==========================================+==========================================+
   | .. code-block:: ini                      | .. code-block:: ini                      |
   |                                          |  :emphasize-lines: 1,2,6,7,8             |
   |                                          |                                          |
   |  location /ws-mds1 {                     |  location /wssip-mds1 {                  |
   |                                          |   auth_request /validatetoken;           |
   |                                          |                                          |
   |   proxy_pass http://10.32.0.201:5039/ws; |   proxy_pass http://10.32.0.201:5039/ws; |
   |   ...                                    |   ...                                    |
   |   proxy_connect_timeout 60m;             |   proxy_connect_timeout 1m;              |
   |   proxy_read_timeout 60m;                |   proxy_read_timeout 5m;                 |
   |   proxy_send_timeout 60m;                |   proxy_send_timeout 5m;                 |
   |   keepalive_timeout 180s;                |   keepalive_timeout 180s;                |
   |  }                                       |  }                                       |
   +------------------------------------------+------------------------------------------+


On MDS
------

* You must also install & configure the mail on MDS to be able to have the email notification for voicemail on MDS (see XiVO Main part above):

  #. Install and configure mail

     .. code-block:: bash

        export DEBIAN_FRONTEND=noninteractive
        apt-get install --yes postfix
        XIVO_HOST=$(grep XIVO_HOST /etc/docker/mds/custom.env | awk -F "=" '/1/ {print $2}')
        postconf -e "relayhost = $XIVO_HOST"
        myorigin=$(dnsdomainname)
        [ -z "${myorigin}" ] && myorigin=$(hostname --short)
        touch /etc/mailname
        echo "${myorigin}" > /etc/mailname
        postconf -e "myorigin = /etc/mailname"
        postfix reload

  #. Then you must follow the :ref:`MDS install doc <xds_mds-mail-conf>` to finish the mail relay configuration.
