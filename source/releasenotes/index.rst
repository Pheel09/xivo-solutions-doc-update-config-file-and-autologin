.. _xivosolutions_release:

*************
Release Notes
*************

.. _izar_release:

Izar
====

Below is a list of *New Features* and *Behavior Changes* compared to the previous LTS version, Helios (2021.15).

New Features
------------

Behavior Changes
----------------

.. _izar_release_deprecations:

Deprecations
------------

This release deprecates:

* `LTS Callisto (2019.05) <https://documentation.xivo.solutions/en/2019.05/>`_: This version is no longer supported.
  No bug fixes, no security update will be provided for this release.


Upgrade
-------

.. _upgrade_lts_manual_steps:

**Manual steps for LTS upgrade**

.. warning:: **Don't forget** to read carefully the specific steps to upgrade from another LTS version

.. toctree::
   :maxdepth: 1

   upgrade_from_five_to_polaris
   upgrade_from_polaris_to_aldebaran
   upgrade_from_aldebaran_to_borealis
   upgrade_from_borealis_to_callisto
   upgrade_from_callisto_to_deneb
   upgrade_from_deneb_to_electra
   upgrade_from_electra_to_freya
   upgrade_from_freya_to_gaia
   upgrade_from_gaia_to_helios
   upgrade_from_helios_to_izar


**Generic upgrade procedure**

Then, follow the generic upgrade procedures:

* :ref:`XiVO PBX upgrade procedure <upgrade>`
* :ref:`XiVO CC upgrade procedure <upgrade_cc>`
* :ref:`XDS upgrade procedure <upgrade_xds>`

Izar Bugfixes Versions
========================

Components version table
------------------------

Table listing the current version of the components.

+----------------------+----------------+
| Component            | current ver.   |
+======================+================+
| **XiVO**                              |
+----------------------+----------------+
| XiVO PBX             | 2021.15.04     |
+----------------------+----------------+
| config_mgt           | 2021.15.05     |
+----------------------+----------------+
| db                   | 2021.15.05     |
+----------------------+----------------+
| outcall              | 2021.15.00     |
+----------------------+----------------+
| db_replic            | 2021.15.00     |
+----------------------+----------------+
| nginx                | 2021.15.01     |
+----------------------+----------------+
| webi                 | 2021.15.05     |
+----------------------+----------------+
| switchboard_reports  | 2021.15.00     |
+----------------------+----------------+
| **XiVO CC**                           |
+----------------------+----------------+
| elasticsearch        | 7.14.0         |
+----------------------+----------------+
| kibana               | 7.14.0         |
+----------------------+----------------+
| logstash             | 2021.15.00     |
+----------------------+----------------+
| mattermost           | 2021.15.00     |
+----------------------+----------------+
| nginx                | 2021.15.00     |
+----------------------+----------------+
| pack-reporting       | 2021.15.00     |
+----------------------+----------------+
| pgxivocc             | 2021.15.00     |
+----------------------+----------------+
| recording-rsync      | 1.0            |
+----------------------+----------------+
| recording-server     | 2021.15.01     |
+----------------------+----------------+
| spagobi              | 2021.15.00     |
+----------------------+----------------+
| xivo-full-stats      | 2021.15.00     |
+----------------------+----------------+
| xuc                  | 2021.15.06     |
+----------------------+----------------+
| xucmgt               | 2021.15.06     |
+----------------------+----------------+
| **Edge**                              |
+----------------------+----------------+
| edge                 | 2021.15.01     |
+----------------------+----------------+
| nginx                | 2021.15.01     |
+----------------------+----------------+
| kamailio             | 2021.15.06     |
+----------------------+----------------+
| coturn               | 2021.15.00     |
+----------------------+----------------+
| **Meeting Rooms**                     |
+----------------------+----------------+
| meetingroom          | 2021.15.06     |
+----------------------+----------------+
| web-jitsi            | 2021.15.06     |
+----------------------+----------------+
| jicofo-jitsi         | 2021.15.06     |
+----------------------+----------------+
| prosody-jitsi        | 2021.15.06     |
+----------------------+----------------+
| jvb-jitsi            | 2021.15.06     |
+----------------------+----------------+
| jigasi-jitsi         | 2021.15.06     |
+----------------------+----------------+
| **IVR**                               |
+----------------------+----------------+
| ivr-editor           | 2021.15.06     |
+----------------------+----------------+


Izar Intermediate Versions
============================

.. toctree::
   :maxdepth: 2

   izar_iv
