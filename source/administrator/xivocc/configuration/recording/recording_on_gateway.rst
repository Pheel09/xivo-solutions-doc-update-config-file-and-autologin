.. _recording_gw_configuration:

********************
Recording on Gateway
********************

Recording can be enabled on a gateway instead of the XiVO PBX where the users, queues and other objects are configured. This architecture will allow to off-load the recording process to a gateway server.
Note that in this architecture not all the recording features are available - see :ref:`recording_gw_features` section.

.. figure:: recording_on_gw.png

.. contents:: :local:

.. important:: This page describes the case when recording is activated on the **XiVO Gateway**.
   If you want to configure it on the *XiVO PBX* you MUST follow the :ref:`recording_configuration` guide.
   **Beware that you cannot use both.**

Activate Recording on Gateway
=============================

To configure recording there are two steps to follow on **XiVO Gateway**:

#. Add link towards Recording Server,
#. and then enable recording via subroutines


1. Add link towards Recording Server
------------------------------------

.. note:: Steps to be done on **XiVO Gateway**

The first step is to configure the link towards the Recording Server by running the configuration script:

.. code-block:: bash

  xivocc-recording-config

During the configuration, you will be asked for :

* the Recording Server IP (e.g. 192.168.0.2)
* the *XiVO Gateway* name (it must not contain any space or "-" character).
  If you configure more than one *XiVO PBX* on the same Recording Server, you must give a different name to each of them.

After having configured the recording, you have to enable via sub-routines. See below.

2. Enable recording
-------------------

.. note:: Steps to be done on **XiVO Gateway**

You must activate the recording by adding subroutines:

* Create the subroutines in the file :file:`/etc/asterisk/extensions_extra.d/gateway-recording.conf`:

  .. code-block:: bash

    [gateway-recording-outcall]
    exten = s,1,NoOp(=== Recording calls from Gateway to XiVO PBX ===)
    same = n,Set(gateway_name=xivogw1)
    same = n,Set(recordingid=${gateway_name}-${UNIQUEID})
    same = n,SIPAddHeader(X-Xivo-Recordingid: ${recordingid})
    same = n,Set(MONITOR_EXEC=/usr/bin/xivocc-synchronize-file)
    same = n,Monitor(,/var/spool/xivocc-recording/audio/${recordingid},m)
    same = n,Return()

    [gateway-recording-incall]
    exten = s,1,NoOp(=== Recording calls from Gateway to Provider ===)
    same = n,Set(recordingid=${SIP_HEADER(X-Xivo-Recordingid)})
    same = n,GotoIf($["${recordingid}" = ""]?norecord:)
    same = n,Set(MONITOR_EXEC=/usr/bin/xivocc-synchronize-file)
    same = n,Monitor(,/var/spool/xivocc-recording/audio/${recordingid},m)
    same = n(norecord),Return()

* Activate subroutines:

  * Configure the ``gateway-recording-outcall`` on the Outgoing call rule towards the XiVO PBX
  * Configure the ``gateway-recording-incall`` on the Outgoing call rule towards the Provider


Activate Recording on XiVO PBX
==============================

.. note:: Step to be done on the *XiVO PBX*

On the XiVO PBX you need to use specific subroutines instead of the default ones.

* Create the subroutines in the file :file:`/etc/asterisk/extensions_extra.d/xivo-gateway-recording.conf`:

  .. code-block:: bash

    ; This subroutine is to be used only when recording is set up on gateway
    [xivocc-incall-recording-viagw]
    exten = s,1,NoOp(=== Recording incoming calls (XiVO PBX) ===)
    same = n,Set(XIVO_RECORDINGID=${SIP_HEADER(X-Xivo-Recordingid)})
    same = n,Set(DO_RECORD=true)
    same = n,System(test -f /usr/share/asterisk/agi-bin/xivocc-determinate-record-incall)
    same = n,GotoIf($["${SYSTEMSTATUS}" = "SUCCESS"]?:noagi)
    same = n,AGI(xivocc-determinate-record-incall)
    same = n(noagi),GotoIf($["${DO_RECORD}" = "true"]?:norecord)
    same = n,CelGenUserEvent(ATTACHED_DATA,recording=${XIVO_RECORDINGID})
    same = n(norecord),Return()

    [xivocc-outcall-recording-viagw]
    exten = s,1,NoOp(=== Outgoing Call Recording (XiVO PBX) ===)
    same = n,Set(DO_RECORD=true)
    same = n,System(test -f /usr/share/asterisk/agi-bin/xivocc-determinate-record-outcall)
    same = n,GotoIf($["${SYSTEMSTATUS}" = "SUCCESS"]?:noagi)
    same = n,AGI(xivocc-determinate-record-outcall)
    same = n(noagi),GotoIf($["${DO_RECORD}" = "true"]?:norecord)
    same = n,Set(ipbx_name=xivo)
    same = n,Set(XIVO_RECORDINGID=${ipbx_name}-${UNIQUEID})
    same = n,SIPAddHeader(X-Xivo-Recordingid: ${XIVO_RECORDINGID})
    same = n,CelGenUserEvent(ATTACHED_DATA,recording=${XIVO_RECORDINGID})
    same = n(norecord),Return()

* Activate the subroutines:

  * Configure the ``xivocc-incall-recording-viagw`` on the Incoming calls/Queues in your XiVO PBX
  * Configure the ``xivocc-outcall-recording-viagw`` on the Outgoing call rule towards the Provider


.. _recording_gw_features:

Recording On Gateway Features
=============================

Available Features
------------------

When you activate the recording on a XiVO Gateway all features of the recording server are available:

* :ref:`recording_search_listen`
* :ref:`recording_disk_space`
* :ref:`recording_access`
* :ref:`recording_filtering`

Unavailable Features
--------------------

When you activate the recording on a XiVO Gateway these features **are not** available:

* :ref:`automatic_stop_start_rec_on_queues`
* :ref:`stop_recording_upon_ext_xfer`
* :ref:`Recording Control on CC Agent <agent_recording>` *except if you follow the* :ref:`recording_gw_activate_rec_control` *procedure*


.. _recording_gw_activate_rec_control:

Activate the Recording Control on CC Agent
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

When the recording is activated on the XiVO Gateway you can activate the Recording Control feature by following this specific procedure :

**Allow the xucserver to connect to your XiVO Gateways:**

#. Copy the file :file:`/etc/asterisk/manager.d/02-xivocc.conf` from the XiVO PBX to the same folder on the gateway.

**Configure the XiVO Gateway as a Media Server**

#. On your XiVO PBX, add your *XiVO Gateway* as a Media Server in :menuselection:`Configuration --> Media servers`

   .. important:: After this step the xucserver will try to connect to the *XiVO Gateway* asterisk AMI

#. On your **XiVO PBX**, configure the provider trunks: you need to add on your *XiVO PBX* the **same** provider trunk that is already
   created on your *XiVO Gateway* BUT with selecting, for the field :guilabel:`Media Server` the *XiVO Gateway* you added.


**Configure the xuc**

#. Disable the feature Stop recording upon external transfer feature: see :ref:`stop_recording_upon_ext_xfer_conf`
#. Disable the Automatic Stop/Start Recording On Queues feature : see :ref:`automatic_stop_start_rec_on_queues_conf`
#. Restart the xuc::

    xivocc-dcomp up -d
