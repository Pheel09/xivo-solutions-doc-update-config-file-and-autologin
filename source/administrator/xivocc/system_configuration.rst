********************
System Configuration
********************

.. _nginx_path_distribution:

Nginx path distribution
=======================

XiVO CC services can be installed on separate servers, but they can be opened through nginx
as if they were running on the same server:

+--------------+-------------------------------+---------------------------------------------------------------+
| Service      | URL                           | Target URL (should not be used)                               |
+==============+===============================+===============================================================+
| Fingerboard  | \https://XUC_HOST/fingerboard | \http://XUC_HOST/                                             |
+--------------+-------------------------------+---------------------------------------------------------------+
| UC Assistant | \https://XUC_HOST/            | \http://XUC_HOST/                                             |
+--------------+-------------------------------+---------------------------------------------------------------+
| CC Agent     | \https://XUC_HOST/ccagent     | \http://XUC_HOST:8070/ccagent                                 |
+--------------+-------------------------------+---------------------------------------------------------------+
| CC Manager   | \https://XUC_HOST/ccmanager   | \http://XUC_HOST:8070/ccmanager                               |
+--------------+-------------------------------+---------------------------------------------------------------+
| Config Mgt   | \https://XUC_HOST/configmgt   | \http://XIVO_HOST:9100/configmgt                              |
+--------------+-------------------------------+---------------------------------------------------------------+
| Kibana       | \https://XUC_HOST/kibana      | \http://XUC_HOST/kibana                                       |
+--------------+-------------------------------+---------------------------------------------------------------+
| Recording    | \https://XUC_HOST/recording   | \http://RECORDING_SERVER_HOST:RECORDING_SERVER_PORT/recording |
+--------------+-------------------------------+---------------------------------------------------------------+
| SpagoBI      | \https://XUC_HOST/SpagoBI     | \http://REPORTING_HOST:9500/SpagoBI                           |
+--------------+-------------------------------+---------------------------------------------------------------+


ACD Outgoing Calls For Call Blending
====================================

Use the following only if you want to use "Least Recent" call distribution strategy and
that outbound agent calls have to be taken into account by the distribution strategy.

By default, when your agents process incoming and outgoing calls, the call distribution will not take into account agents
which are in outgoing calls in the *least recent* call strategy and at the end of an outgoing call there is no wrapup.
So an agent can be distributed just after an outgoing calls even if another agent is free for a longer time, because the
outgoing call is not taken into account by the distribution strategy.

You will find below how to improve that.

XiVO-CC agent can make outgoing calls through an outgoing queue. This brings the statistics and
supervision visualization for outgoing ACD calls. However, some special configuration steps are
required. The outgoing calls must be dialed from CC Agent application to use this feature.

Configuration Steps
-------------------

* You need to create an outgoing queue with

  * in tab *General*:

    * :guilabel:`Name`: starting with 'out', e.g. outbound,
    * :guilabel:`Number`: some number
    * :guilabel:`Music On-Hold`: None
    * :guilabel:`Preprocess subroutine`: xuc_outcall_acd
  * in tab *Application*:

    * :guilabel:`Ringing Time`: 0
    * :guilabel:`Ring instead of On-Hold music`: activated

* Agent will have to be logged on this queue

How to check correct configuration
----------------------------------

Check if agent is logged in the outbound queue:

.. code-block:: bash

      jyl-rennes*CLI> queue show outbound
      outbound has 0 calls (max unlimited) in 'ringall' strategy (0s holdtime, 0s talktime), W:0, C:0, A:0, SL:0.0% within 0s
      Members:
        Agent/2500 (Local/id-19@agentcallback from SIP/ihvbur) (ringinuse disabled) (dynamic) (Not in use) (skills: agent-19) has taken no calls yet
      No Callers


Check the skills attached to the agent by displaying it's agent group:

.. code-block:: bash

      jyl-rennes*CLI> queue show skills groups agent-19
      Skill group 'agent-19':
        - agent_19       : 100
        - agent_no_2500  : 100
        - genagent       : 100

If the agent dials an outbound call of more than 6 digits (default) you should see the internal queue statistics updated.
The agent's state should be "(in call)" for ongoing call and when the call ends, the number of taken calls should be incremented:

.. code::

    jyl-rennes*CLI> queue show outbound
    outbound has 0 calls (max unlimited) in 'ringall' strategy (0s holdtime, 34s talktime), W:0, C:1, A:0, SL:100.0% within 0s
       Members:
          Agent/2500 (Local/id-19@agentcallback from SIP/ihvbur) (ringinuse disabled) (dynamic) (Not in use) (skills: agent-19) has taken 1 calls (last was 1 secs ago)
       No Callers


Once done, calls requested by an agent through the Cti.js with more than 6 digits are routed via
the outgoing queue. You can change the number of digits using the parameter xuc.outboundLength
in the XuC's configuration.

.. _ldap-xuc-authentication:

Login Timeout Configuration
===========================

When configuring external service as authentication provider, you may have trouble to login if the external system is slow. To overcome this issue, you can increase the default timeout (5000 milliseconds default) by setting the `LOGIN_TIMEOUT_MS` parameter in the :file:`custom.env` of your xivocc installation. This value is the delay in milliseconds before aborting a login attempt.

.. code-block:: shell

  LOGIN_TIMEOUT_MS=7000


Ldap Authentication
===================

Xuc
---

Configure LDAP authent for CCmanager, UC Assistant and CC Agent

You need to include in the :file:`docker-xivocc.yml` file a link to a specific configuration file by adding in xuc section a specific volume
and an environment variable to specify the alternate config file location

.. code-block:: yaml
  :emphasize-lines: 6,9

  xuc:
    image: ...

    environment:
    - ...
    - CONFIG_FILE=/conf/xuc.conf

    volumes:
    - /etc/docker/xuc:/conf


Edit in :file:`/etc/docker/xuc/` folder a configuration file named :file:`xuc.conf` to add ldap configuration (empty by default)


.. code-block:: ini

   include "application.conf"

   authentication {
     ldap {
       managerDN = "uid=company,ou=people,dc=company,dc=com"      # user with read rights on the whole LDAP
       managerPassword = "xxxxxxxxxx"                             # password for this user
       url = "ldap://ldap.company.com:389"                        # ldap URI
       searchBase = "ou=people,dc=company,dc=com"                 # ldap entry to use as search base
       userSearchFilter = "uid=%s"                                # filter to use to search users by login, using a string pattern
     }
   }

Recreate the container : ``xivocc-dcomp up -d xuc``

.. _kerberos-configuration:

Kerberos Authentication
=======================

To enable Kerberos authentication and single sign on feature, you need to have an existing Kerberos infrastructure with a
Key Distribution Center and a Ticket Granting Service. You need to be able to create a service, construct a kerberos server configuration and export a keytab to perform the following configuration. This service must be on the kerberos realm used by your users and must match the dns name of the server hosting the XUC server (or the nginx reverse proxy server if you use one). For example, assuming you have a realm named ``MYDOMAIN``, you can create a service named ``HTTP/xuc.mydomain`` and a dns entry for ``xuc.mydomain`` pointing the server hosting the XUC.

.. warning::

   The created domain name must be trusted by the user's browser.

Prerequisites
-------------

* Create a service for the XUC host, for example:

  .. code-block:: shell

                  addprinc HTTP/xuc.mydomain

* Export the keytab file, for example:

  .. code-block:: shell

                  ktadd -k xuc.keytab HTTP/xuc.mydomain


.. warning::

   The bash commands detailed here are for demonstration only and needs to be adapted to your specific environment. It shows how to create a service for the XUC Server named HTTP/xuc, associated to the example realm ``mydomain``.

   Only the following encryption types are supported by XiVOCC:

   - aes256-cts-hmac-sha1-96
   - arcfour-hmac
   - des3-cbc-sha1
   - des-cbc-crc


XiVOCC Configuration
--------------------

* Copy the previously generated ``xuc.keytab`` keytab file to the server hosting the XUC docker container, for example: ``/etc/docker/kerberos/xuc.keytab``.
* Create or edit the file ``/etc/krb5.conf`` on the server hosting the XUC docker container and change settings according to your kerberos environment. For example, the file may contain (name and ip addresses must match your kerberos environment):

  .. code-block:: ini

     [libdefaults]
         default_realm = MYDOMAIN

     [realms]
         MYDOMAIN = {
             kdc = 172.17.0.14
             admin_server = 172.17.0.14
         }

* Edit the docker compose file ``/etc/docker/compose/docker-xivocc.yml`` to add the following configuration in the xuc section (file name, service name, password may differ on your setup):

  .. code-block:: dockerfile


                  xuc:
                    # ...
                    environment:
                    - JAVA_OPTS=-Dsecured.krb5.principal=HTTP/xuc.mydomain -Dsecured.krb5.password=xuc -Dsecured.krb5.keyTab=/etc/kerberos/xuc.keytab

                    # ...

                    volumes:
                    - /etc/docker/kerberos:/etc/kerberos
                    - /etc/krb5.conf:/etc/krb5.conf


* Enable ``Single Sign On`` on the Agent, Manager, Web and Desktop application interface. Change the value of the following environment variables in the `/etc/docker/compose/custom.env`:

  .. code-block:: ini

     # ...
     USE_SSO=true
     XUC_HOST=xuc.mydomain
     # ...


Browser configuration
---------------------

The created domain name must be trusted by the user’s browser.

For Chrome (windows):

- Internet Option : Add domain with protocol to the list of trusted sites : http://xuc.mydomain (and/or https://xuc.mydomain ).


.. warning::

   Kerberos authentication on Chrome is only available on Microsoft Windows.

For Firefox:

- Go to ``about:config``
- add domain (without protocol) to the ``network.negotiate-auth.delegation-uris`` entry (ie. ``xuc.mydomain``).
- add domain (without protocol) to the ``network.negotiate-auth.trusted-uris`` entry (ie. ``xuc.mydomain``).

.. _cas-sso-configuration:

CAS SSO Authentication
=======================

To enable CAS authentication and single sign on feature, you need to have an existing CAS infrastructure. You need to be able to create a service for the XiVOCC environment.

.. warning::

   The CAS authentication server must be accessible from the user and the server hosting the XiVOCC containers.
   CAS Server users' username must match the XiVO username to allow login on the XiVOCC applications.
   The CAS server must support at least CAS Protocol version 2.0.


XiVOCC Configuration
--------------------

* Edit the docker compose file ``/etc/docker/compose/docker-xivocc.yml`` to add the following configuration in the xuc section (use your CAS server URL instead of ``https://cas-server.example.org/cas`` and set ``CAS_LOGOUT_ENABLE`` to ``true`` if you want to logout from CAS when logging out from the application):

  .. code-block:: dockerfile

                  xucmgt:
                    # ...
                    environment:
                    - CAS_SERVER_URL=https://cas-server.example.org/cas
                    - ...

                    # ...

                  xuc:
                    # ...
                    environment:
                    - CAS_SERVER_URL=https://cas-server.example.org/cas
                    - CAS_LOGOUT_ENABLE=false
                    - ...

                    # ...

* Recreate and start the XiVOCC environment:

  .. code-block:: shell

     xivocc-dcomp up -d

.. _oidc-sso-configuration:

OpenID Connect (OIDC) SSO Authentication
========================================

To enable OIDC authentication and single sign on feature, you need to have an existing OpenID Connect 1.0 infrastructure. You need to be able to create a realm and a client for the XiVOCC environment.

.. warning::

  * The OIDC authentication server must be accessible from the user and the server hosting the XiVOCC containers.
  * OIDC Server users' username must match the XiVO username to allow login on the XiVOCC applications.
  * Audience field in access_token must contain your clientId


XiVOCC Configuration
--------------------

* Edit the docker compose file ``/etc/docker/compose/docker-xivocc.yml`` to add the following configuration in the xuc section (use your OIDC server URL instead of ``https://oidc-server.example.org/auth/realms/myrealm`` and set ``ENABLE_OIDC`` to ``true`` if you want to use this authentication as default one):

 .. code-block:: dockerfile

                 xucmgt:
                   # ...
                   environment:
                   - OIDC_SERVER_URL=https://oidc-server.example.org/auth/realms/myrealm
                   - OIDC_CLIENT_ID=myClientId
                   - ...

                   # ...

                 xuc:
                   # ...
                   environment:
                   - ENABLE_OIDC=true
                   - OIDC_SERVER_URL=https://oidc-server.example.org/auth/realms/myrealm
                   - OIDC_CLIENT_ID=myClientId1,myClientId2
                   - ...

                   # ...

.. note::
  Additional Xuc Configuration:

  * **OIDC_AUDIENCE** : to define additional check on *aud* access_token field, *default* is set to *OIDC_CLIENT_ID*
  * **OIDC_USERNAME_FIELD** : to define which field in access_token to use to map XiVO username , *default* is *preferred_username*


* Recreate and start the XiVOCC environment:

 .. code-block:: shell

    xivocc-dcomp up -d


.. _disabling-xivo-authentication:

Disabling XiVO based authentication
===================================

If you configured another kind of authentication, like :ref:`ldap-xuc-authentication` or :ref:`kerberos-configuration` for example, you can enforce this authentication mechanism by preventing fall back on the XiVO authentication mechanism. In order to achieve that, you need to perform the following configuration.

You need to include in the :file:`docker-xivocc.yml` file a link to a specific configuration file by adding in xuc section a specific volume
and an environment variable to specify the alternate config file location

.. code-block:: yaml
  :emphasize-lines: 6,9

  xuc:
    image: ...

    environment:
    - ...
    - CONFIG_FILE=/conf/xuc.conf

    volumes:
    - /etc/docker/xuc:/conf


Edit in :file:`/etc/docker/xuc/` folder a configuration file named :file:`xuc.conf` to disable the xivo based authentication:


.. code-block:: ini

   include "application.conf"

   authentication {
      xivo = ""
   }

Recreate the containers according to the new configuration : ``xivocc-dcomp up -d``

.. _allow_xuc_login:

Allowing xuc user to login to API or Applications
=================================================

By default, the 'xuc' user can only be used internaly by the xucserver to connect to the xivo. We strongly advise you shouldn't change this behavior but if you need to enable it, you can do it by setting the following variable in the :file:`custom.env` file of your xivocc server.

::

   AUTH_PREVENT_XUC_LOGIN=false

.. _nginx-trusted-certificate:

Install trusted certificate for nginx
=====================================

To install a trusted certificate for the nginx reverse proxy instead of the self signed certificate, follow the following instructions:

* in directory ``/etc/docker/nginx/ssl`` replace content of files

  * :file:`xivoxc.crt`,
  * :file:`xivoxc.csr`,
  * :file:`xivoxc.key` while keeping filenames unchanged
* reload nginx container by command::

   xivocc-dcomp reload nginx

* you should then check that the certificate chain is complete (especially for the XiVO Mobile Assistant) - see :ref:`nginx-complete-cert-chain`.


.. _nginx-complete-cert-chain:

What is a complete certificate chain
------------------------------------

You can check the server certificate chain by using the following web site https://www.ssllabs.com/ssltest/analyze.html which will warn you if there is an error with the certificate (``Chain issues - Incomplete``).

When a client application (browser or mobile application) checks a certificate for a web site, it checks the received certificate is issued by a known certificate authority and matches the web site domain name. But sometimes, the certificate is not issued by a root certificate authority but by an intermediate authority.

Here is an example of a such a certificate chain:

.. code-block:: none

    GeoTrust Global CA
    |--> RapidSSL SHA256 - CA - G3
         |--> *.company.com


The possible problem here is that even if the browser knows the root authority, it is unaware of the intermediate one. The solution is to create a bundle of the complete certificate chain by concatenating the certificates of all parties (root, intermediate & site). Please see http://nginx.org/en/docs/http/configuring_https_servers.html#chains for more information.

Mobile Assitant
===============

If using an HTTPS connection for the XiVO Mobile Assistant, you must use a trusted certificate with a complete certification chain, see :ref:`nginx-trusted-certificate`.
