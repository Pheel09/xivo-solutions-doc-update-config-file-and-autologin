.. _nginx:

*****
Nginx
*****

XiVO uses nginx docker container as a web server and reverse proxy.

In its default configuration, the nginx server listens on port TCP/80 and TCP/443 and allows these
services to be used:

* web interface (webi container)
* API documentation (xivo-swagger-doc)

XiVO UC Add-on
==============

If you install the :ref:`UC Add-on <xivouc_addon>` then the nginx container also listen on TCP/8443 and serves
the UC application (via xuc/xucmgt container).


Notes
=====

Customization of the services to be used through the nginx container are not supported.
