.. _gcu_releasenotes:

*****************
GCU Release Notes
*****************

.. contents::

2020.18
=======

Below is a list of *New Features* and *Behavior Changes* in the Centralized User Management interface introduced in the 2020.18 version.

New Features
------------

**Templates**

* It is possible to choose WebRTC or Unique Account as a peer type - see :ref:`xcu_create_template`

**Entities**

* When creating an entity we can now choose a Media Server - see :ref:`xcu_create_entity`

**Users**

* After creating a user, a confirmation message is displayed - see :ref:`User confirmation message <xcu_user_creation>`
* Internal number for a user can be now chosen by typing and narrowing the available numbers result. The number is selected by clicking on it or by pressing tab key.

Behavior Changes
----------------

**Entities**

* Removed *not routed* mode - see :ref:`routing modes <routing_modes>`


